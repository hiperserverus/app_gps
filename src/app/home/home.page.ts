import { Component, ViewChild, OnInit, EventEmitter} from '@angular/core';
import { UserService } from '../services/user.service';
import { LocationService } from '../services/location.service';
//  import { mapboxgl} from 'mapbox-gl';

declare var mapboxgl;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  stopLocation = new EventEmitter<any>();

  @ViewChild('mapa', {static:true}) mapa: any;
  coords: any ;
  user: any = {};
  map: any;
  marker: any;
  lat: any;
  lng:any;
  source:any;
  popup: any;

  constructor(
              private _userService: UserService,
              private _locationService: LocationService) {
  }

  ngOnInit() {

    this._locationService.inicializateUser();

    this._locationService.getLocation();

    this._userService.verificationUser('1').then(resp =>{
        console.log('DATOS USER FIRESTORE', resp);
        this.user = resp;
        this.createMap();
    })
    .catch(err => {
      console.log(err);
    });

    this._locationService.user.valueChanges().subscribe(data => {
      console.log('DATOS CHANGED FIRESTORE', data);
      this.user = data;
      this.lat = Number(this.user.lat);
      this.lng = Number(this.user.lng);
      this.map.flyTo({
        center: [this.lng, this.lat]
      });

   

      this.marker.remove();
      this.popup.remove();

      this.marker = new mapboxgl.Marker({color: 'red'})
      .setLngLat([this.lng, this.lat - 0.00035])
      .addTo( this.map );

      this.popup = new mapboxgl.Popup({ closeOnClick: false })
      .setLngLat([this.lng, this.lat])
      .setHTML('<h1>Usuario 1</h1>')
      .addTo(this.map);

      


    });
    
  }

  createMap() {
    // const lnglat = this.coords.split(',');


    this.lat = Number(this.user.lat);
    this.lng = Number(this.user.lng);

    
    const cr = [this.lng , this.lat];

    mapboxgl.accessToken = 'pk.eyJ1IjoiaGlwZXJzZXJ2ZXJ1cyIsImEiOiJjazVjcnF2ZDQxb3gwM2ttbWswaG55cTR0In0.8ak7njczgbaoPFWAvnbi9g';
    this.map = new mapboxgl.Map({
    container: this.mapa.nativeElement,
    style: 'mapbox://styles/mapbox/streets-v11',
     center: [this.lng , this.lat],
     zoom: 15
    });

    this.map.addControl(new mapboxgl.NavigationControl());

    this.popup = new mapboxgl.Popup({ closeOnClick: false })
      .setLngLat([this.lng, this.lat])
      .setHTML('<p>Usuario 1</p>')
      .addTo(this.map);

     this.marker = new mapboxgl.Marker({color: 'red'})
          .setLngLat([this.lng , this.lat - 0.00035])
          .addTo( this.map );


 

    //  this.changeCarged();



  }

  followStoped() {
    this._locationService.stopLocation();

    try{
      this._userService.watchDocFirestore.unsubscribe();
    } catch(e) {
      console.log(JSON.stringify(e));
    }
   
  }

  // changeCarged() {

  //   this.map.on('load', (event) => {


  //     this.map.addSource('national-park', {
  //       'type': 'geojson',
  //       'data': {
  //       'type': 'FeatureCollection',
  //       'features': [
       
  //       {
  //       'type': 'Feature',
  //       'geometry': {
  //       'type': 'Point',
  //       'coordinates': [this.lng , this.lat]
  //       }
  //       },
  //       ]
  //       }
  //       });
         
         
  //       this.map.addLayer({
  //       'id': 'park-volcanoes',
  //       'type': 'symbol',
  //       'source': 'national-park',
        
  //       'layout': {
  //         'text-field': "usuario 1",
  //         'text-size': 15,
  //         'text-transform': 'lowercase',
  //         'icon-image': 'harbor-15',
  //         'text-offset': [0, 1.5]
  //       },
  //       'paint': {
  //         'text-color': '#c9c9c9',
  //         'text-halo-color': '#fff',
  //         'text-halo-width': 2
  //       },
  //       'filter': ['==', '$type', 'Point']
  //       });


  //     //FIN LOAD
  // });

  // }

 }
 

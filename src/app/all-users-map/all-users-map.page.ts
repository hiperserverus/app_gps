import { Component, OnInit, ViewChild } from '@angular/core';
import { LocationService } from '../services/location.service';
import { Observable } from 'rxjs/internal/Observable';

declare var mapboxgl;

@Component({
  selector: 'app-all-users-map',
  templateUrl: './all-users-map.page.html',
  styleUrls: ['./all-users-map.page.scss'],
})
export class AllUsersMapPage implements OnInit {

  @ViewChild('mapa', {static:true}) mapa: any;

  users: any;
  map: any;
  markers: any[] = [];
  lat = 0;
  lng = 0;
  
  popup:any;

  init = false;

  constructor( private _locationService: LocationService) { }

  ngOnInit() {

   this._locationService.allUsers.subscribe(resp => {
     this.users = resp;
     console.log('USERS', resp);

     if (!this.init) {
       this.lat = resp[0].lat;
       this.lng = resp[0].lng;
       this.init = true;
       console.log('CREANDO MAPA', resp);
       this.createMap();

     }else {

      for(let i = 0; i < this.users.length; i++) {

        console.log('MARKERS UPDATE');
  
        // this.popup = new mapboxgl.Popup({ closeOnClick: false })
        // .setLngLat([user.lng, user.lat])
        // .setHTML(`<p>${user.name}</p>`)
        // .addTo(this.map);

        this.markers[i].remove();
  
       this.markers[i] = new mapboxgl.Marker({color: 'red'})
            .setLngLat([this.users[i].lng , this.users[i].lat - 0.00035])
            .addTo( this.map );
  
      
  
      }
     }
   }
    );
  }

  createMap() {
    // const lnglat = this.coords.split(',');


    this.lat = Number(this.lat);
    this.lng = Number(this.lng);

    
    const cr = [this.lng , this.lat];

    mapboxgl.accessToken = 'pk.eyJ1IjoiaGlwZXJzZXJ2ZXJ1cyIsImEiOiJjazVjcnF2ZDQxb3gwM2ttbWswaG55cTR0In0.8ak7njczgbaoPFWAvnbi9g';
    this.map = new mapboxgl.Map({
    container: this.mapa.nativeElement,
    style: 'mapbox://styles/mapbox/streets-v11',
     center: [this.lng , this.lat],
     zoom: 12
    });

    this.map.addControl(new mapboxgl.NavigationControl());

    for(let i = 0; i < this.users.length; i++) {

      console.log('entro');

      // this.popup = new mapboxgl.Popup({ closeOnClick: false })
      // .setLngLat([user.lng, user.lat])
      // .setHTML(`<p>${user.name}</p>`)
      // .addTo(this.map);

     this.markers[i] = new mapboxgl.Marker({color: 'red'})
          .setLngLat([this.users[i].lng , this.users[i].lat - 0.00035])
          .addTo( this.map );

    

    }

    console.log('MARKERS', this.markers);




 

    //  this.changeCarged();



  }

}

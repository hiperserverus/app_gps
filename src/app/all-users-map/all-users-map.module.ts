import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllUsersMapPageRoutingModule } from './all-users-map-routing.module';

import { AllUsersMapPage } from './all-users-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllUsersMapPageRoutingModule
  ],
  declarations: [AllUsersMapPage]
})
export class AllUsersMapPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllUsersMapPage } from './all-users-map.page';

const routes: Routes = [
  {
    path: '',
    component: AllUsersMapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllUsersMapPageRoutingModule {}

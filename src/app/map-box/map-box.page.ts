import { Component, OnInit, ViewChild } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { GeoJson, FeatureCollection } from '../interfaces/map';
import { MapService } from '../services/map.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-map-box',
  templateUrl: './map-box.page.html',
  styleUrls: ['./map-box.page.scss'],
})
export class MapBoxPage implements OnInit {

  @ViewChild('mapa', {static:true}) mapa: any;

  map = mapboxgl.Map;
  style = 'mapbox://styles/mapbox/outdoors-v9';
  lat = 37.75;
  lng = -122.41;
  message = 'Hello World!';

  source: any;
  markers: any;

  constructor(private geolocation: Geolocation,
    private _mapService: MapService) { }

  ngOnInit() {
    this.markers = this._mapService.getMarkers();
    console.log('MARKERS', this.markers);
    this.initializeMap()
  }

  private initializeMap() {

    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      const coords = `${ resp.coords.latitude }, ${ resp.coords.longitude }`;
      console.log(resp.coords);

      this.lng = resp.coords.longitude;
      this.lat = resp.coords.latitude;

      this.map.flyTo({
        center: [this.lng , this.lat]
      })
    
     }).catch((error) => {
       console.log('Error getting location', error);
    
     });

     this.buildMap();

  }

  buildMap() {
    this.map = new mapboxgl.Map({
      container: this.mapa.nativeElement,
      style: this.style,
      zoom: 15,
      center: [this.lng , this.lat]
    });

    this.map.addControl(new mapboxgl.NavigationControl());

    this.map.on('click', (event) => {
      const coordinates = [event.lngLat.lng, event.lngLat.lat];
      const newMarker = new GeoJson(coordinates, {message: this.message});
      this._mapService.createMarker(newMarker);
    });

    this.map.on('load', (event) => {
        this.map.addSource('firebase', {
          type: 'geojson',
          data: {
            type: 'FeatureCollection',
            features: []
          }
        })

        this.source = this.map.getSource('firebase');

        this.markers.subscribe(markers => {
          let data = new FeatureCollection(markers);
          this.source.setData(data);
        })

        this.map.addLayer({
          id: 'firebase',
          source: 'firebase',
          type: 'symbol',
          layout: {
            'text-field': '{message}',
            'text-size': 24,
            'text-transform': 'uppercase',
            'icon-image': 'rocket-15',
            'text-offset': [0, 1.5]
          },
          paint: {
            'text-color': '#f16624',
            'text-halo-color': '#fff',
            'text-halo-width': 2
          }
        })
    });




  }

  removeMarker(marker) {
    
    this._mapService.removeMarker(marker.$key);
  }

  flyTo(data: GeoJson) {
    this.map.flyTo({
      center: data.geometry.coordinates
    })
  }

  


}

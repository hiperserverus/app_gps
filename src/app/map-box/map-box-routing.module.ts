import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapBoxPage } from './map-box.page';

const routes: Routes = [
  {
    path: '',
    component: MapBoxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapBoxPageRoutingModule {}

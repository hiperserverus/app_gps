import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapBoxPageRoutingModule } from './map-box-routing.module';

import { MapBoxPage } from './map-box.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapBoxPageRoutingModule
  ],
  declarations: [MapBoxPage]
})
export class MapBoxPageModule {}

export interface Users {
    dni?: string;
    name?: string;
    email?: string;
    phone?: string;
    lat?: string;
    lng?: string;

}
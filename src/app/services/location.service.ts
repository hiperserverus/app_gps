import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Subscription } from 'rxjs/internal/Subscription';
import { Observable } from 'rxjs/internal/Observable';
import { Users } from '../interfaces/users';


@Injectable({
  providedIn: 'root'
})
export class LocationService {

  user: AngularFirestoreDocument<any>;
  watch:Subscription;
   allUsers: Observable<any[]>
  // allUsers: Users[] = [];

  constructor( private geolocation: Geolocation,
                private firestoreDB: AngularFirestore) {

                  this.getAllusers();
}

  inicializateUser() {
    this.user = this.firestoreDB.doc(`/users/1`);
  }

  getLocation() {


      this.geolocation.getCurrentPosition().then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        const coords = `${ resp.coords.latitude }, ${ resp.coords.longitude }`;
        console.log(resp.coords);
        this.user.update({
            lat: resp.coords.latitude,
            lng: resp.coords.longitude
        });
      
       }).catch((error) => {
         console.log('Error getting location', error);
      
       });

                 this.watch = this.geolocation.watchPosition()
                    .subscribe((data) => {
                  // data can be a set of coordinates, or an error (if an error occurred).
                  // data.coords.latitude
                  // data.coords.longitude
            
                  this.user.update({
                  lat: data.coords.latitude,
                  lng: data.coords.longitude
              });
                  const coords = `${ data.coords.latitude }, ${ data.coords.longitude }`;
                  console.log('POSITION UPDATE', data.coords);
                  
                });

 

  }

  stopLocation() {
    try{
      this.watch.unsubscribe();
    } catch(e) {
      console.log(JSON.stringify(e));
    }
    
  }

  getAllusers() {
    this.allUsers = this.firestoreDB.collection('users').valueChanges();

  }

}

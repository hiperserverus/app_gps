import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  watchDocFirestore:Subscription;

  constructor(private firestoreDB: AngularFirestore) { }

  verificationUser( id: string) {

    return new Promise((resolve, reject) => {

      this.watchDocFirestore = this.firestoreDB.doc(`/users/${id}`)
        .valueChanges().subscribe( data => {

          if (data) {
            resolve(data);
            console.log(data);
          } else {
            reject('No se encontro el usuario especificado');
          }
          

          
        })

    });

  }
}

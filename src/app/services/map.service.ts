import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { GeoJson } from '../interfaces/map';
import * as mapboxgl from 'mapbox-gl';


@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private firestoreDB: AngularFirestore) { 
    mapboxgl.accessToken = 'pk.eyJ1IjoiaGlwZXJzZXJ2ZXJ1cyIsImEiOiJjazVjcnF2ZDQxb3gwM2ttbWswaG55cTR0In0.8ak7njczgbaoPFWAvnbi9g';
  }

  getMarkers(): AngularFirestoreCollection <any> {
    return this.firestoreDB.collection('markers')
  }

  createMarker(data:GeoJson) {
    return this.firestoreDB.collection('markers').add(data);
  }

  removeMarker($key: string) {
    return this.firestoreDB.collection('markers').doc($key).delete();
  }
}

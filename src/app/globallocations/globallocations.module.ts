import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GloballocationsPageRoutingModule } from './globallocations-routing.module';

import { GloballocationsPage } from './globallocations.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GloballocationsPageRoutingModule
  ],
  declarations: [GloballocationsPage]
})
export class GloballocationsPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GloballocationsPage } from './globallocations.page';

const routes: Routes = [
  {
    path: '',
    component: GloballocationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GloballocationsPageRoutingModule {}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'map-box',
    loadChildren: () => import('./map-box/map-box.module').then( m => m.MapBoxPageModule)
  },
  {
    path: 'globallocations',
    loadChildren: () => import('./globallocations/globallocations.module').then( m => m.GloballocationsPageModule)
  },
  {
    path: 'all-users-map',
    loadChildren: () => import('./all-users-map/all-users-map.module').then( m => m.AllUsersMapPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
